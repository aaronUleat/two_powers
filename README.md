# Dos Poderes en el Cielo.

## Preguntas?

¿Es la pluralidad divina un problema para ...

* Monoteismo?
* Una sola Deidad?
* Jesus como Dios.

## Respuestas: ¿Qué pasa con el monoteísmo?


* Jehová es un elohim, pero no todos los elohim no son como Jehová
* Jehová es ontológicamente unico.

Jehová es unico, pero existe una segunda figura desde el Antiguo testamento, una figura visible, que al final de todo va terminar siendo Jesus.

Ontologicamente el Padre y Jesus son el mismo, son Jehová.

Deut 6:4

```
Oye, Israel: Jehová nuestro Dios, Jehová uno es.
```

El Judaismo dice que el pluralismo es un problema por que viola la Shemá.

¿Cómo es posible que un judío del primer siglo, tan comprometido con el monoteísmo que elijiria la muerte en lugar de adorar a
el emperador romano o un dios romano, ¿podría abrazar a Jesús de Nazaret como Dios en carne junto al Dios de Israel?
Porque los judíos tenían una creencia en "Dos Poderes en el Cielo" que eran ambos Jehová. Jesús estaba en esa imagen, 
no el Emperador Romano.

La idea de dos Poderes en el cielo, no era nueva para los Judios, lo que es nuevo en el nuevo testamento, es que Jesus
decia ser esa segunda figura. Esta misma figura tambien estaba en el Antiguo Testamento.

Gen 19:24

```
Entonces llovió Jehová sobre Sodoma y sobre Gomorra azufre y fuego de parte de Jehová desde los cielos;
```
En este versiculo la palabra Jeova ocurre dos veces

Amós 4:11

```
 Os trastorné como cuando Dios trastornó a Sodoma y a Gomorra, y 
 fuisteis como tizón escapado del fuego; mas no os volvisteis a mí, dice Jehová.
```

Por que primero esta hablando en primera persona, y despues por que Dios se refiere al mismo en tercera persona?

Gen 22:11-12

```
11 Entonces el ángel de Jehová le dio voces desde el cielo, y dijo:
Abraham, Abraham. Y él respondió: Heme aquí.

12 Y dijo: No extiendas tu mano sobre el muchacho, ni le hagas nada; 
porque ya conozco que temes a Dios, por cuanto no me rehusaste tu hijo, tu único.
```

Por que el ángel de Jehová le dice: `por cuanto no me rehusaste tu hijo`

### Resumen


* Ciertos pasajes del Antiguo Testamento sonaban al oído como si el Dios de Israel fuera "dos"

* Los rabinos tomaron nota de esto y se refirieron a la idea como los "Dos Poderes" en el cielo.

* Esta creencia fue aceptable en el judaísmo hasta alrededor del año 100 d.C., cuando fue declarada herejía.

* Una razón fue la respuesta religiosa al cristianismo.

### Identificando al Segundo Jehová
El angel de Jehová  (Jehová) y el NOMBRE(hashem).


Exo 3:1-4

```
 Apacentando Moisés las ovejas de Jetro su suegro, sacerdote de Madián, llevó las ovejas a través del desierto, 
 y llegó hasta Horeb, monte de Dios.

2 Y se le apareció el Angel de Jehová en una llama de fuego en medio de una zarza; y él miró, 
y vio que la zarza ardía en fuego, y la zarza no se consumía.

3 Entonces Moisés dijo: Iré yo ahora y veré esta grande visión, 
por qué causa la zarza no se quema.

4 Viendo Jehová que él iba a ver, lo llamó Dios de en medio de la zarza, 
y dijo: !!Moisés, Moisés! Y él respondió: Heme aquí.
```

Exo 23:20-22

```
20 He aquí yo envío mi Angel delante de ti para que te guarde en el camino, 
y te introduzca en el lugar que yo he preparado.

21 Guárdate delante de él, y oye su voz; no le seas rebelde; 
porque él no perdonará vuestra rebelión, porque mi nombre está en él.

22 Pero si en verdad oyeres su voz e hicieres todo lo que yo te dijere, 
seré enemigo de tus enemigos, y afligiré a los que te afligieren.
```

Lo que hace diferente a este Angel es que el el nombre de Jehová esta en sobre el.

El nombre es traducido como (Hashem). Es otra manera de refirirse a Jehová

Deut 12:4-11

```
4 No haréis así a Jehová vuestro Dios,

5 sino que el lugar que Jehová vuestro Dios escogiere de entre todas vuestras tribus, 
para poner allí su nombre para su habitación, ése buscaréis, y allá iréis.

6 Y allí llevaréis vuestros holocaustos, vuestros sacrificios, vuestros diezmos,
y la ofrenda elevada de vuestras manos, vuestros votos, vuestras ofrendas voluntarias, 
y las primicias de vuestras vacas y de vuestras ovejas;

7 y comeréis allí delante de Jehová vuestro Dios, y os alegraréis, 
vosotros y vuestras familias, en toda obra de vuestras manos en la cual Jehová tu Dios te hubiere bendecido.

8 No haréis como todo lo que hacemos nosotros aquí ahora, cada uno lo que bien le parece,

9 porque hasta ahora no habéis entrado al reposo y a la heredad que os da Jehová vuestro Dios.

10 Mas pasaréis el Jordán, y habitaréis en la tierra que Jehová vuestro Dios os hace heredar; 
y él os dará reposo de todos vuestros enemigos alrededor, y habitaréis seguros.

11 Y al lugar que Jehová vuestro Dios escogiere para poner en él su nombre, 
allí llevaréis todas las cosas que yo os mando: vuestros holocaustos, vuestros sacrificios, 
vuestros diezmos, las ofrendas elevadas de vuestras manos, y todo lo escogido de los votos que hubiereis prometido a Jehová.
```

El versiculo 5 dice `para poner allí su nombre para su habitación` y el el verso
11 dice `Y al lugar que Jehová vuestro Dios escogiere para poner en él su nombre`

Salmo 20:1,7

```
Jehová te oiga en el día de conflicto;
    El nombre del Dios de Jacob te defienda.
	

7 Estos confían en carros, y aquéllos en caballos;
Mas nosotros del nombre de Jehová nuestro Dios tendremos memoria.
```

El punto aqui es que el nombre (Hashem) es una referencia a sus precencia, a su persona. (Hashen) no son consonantes, es la
personificacion de Dios mismo.

2 Samuel 6:1-2

```
Volvió David a reunir a todos los hombres escogidos de Israel, treinta mil. 
2 Y David se levantó y fue con todo el pueblo que estaba con él a[a] Baala[b] de Judá, para hacer subir desde allí el arca de Dios, 
la cual es llamada por el Nombre, el nombre del Señor de los ejércitos, que está[c] sobre los querubines.
```

La cual es llamada `Hashem`.
El nombre esta en la Arca, ahi fue donde puso su nombre.


Isaías 30:27

```
He aquí, el nombre del Señor viene de lejos;[a]
ardiente es su ira, y denso[b] es su humo[c].
Sus labios están llenos de indignación,
su lengua es como fuego consumidor,
```

Se refiere al el "nombre" como una persona.
"Sus Labios", el "nombre" tiene labios?


Volvamos a  Exod 23

Exo 23:20-22

```
20 He aquí, yo enviaré un ángel delante de ti, para que te guarde en el camino y te traiga al lugar que yo he preparado. 
21 Sé prudente[a] delante de él y obedece su voz; no seas rebelde contra él, pues no perdonará vuestra rebelión, 
porque en él está mi nombre.
22 Pero si en verdad obedeces su voz y haces todo lo que yo digo, entonces seré enemigo de tus enemigos y 
adversario de tus adversarios.
```

La escencia de Dios esta sobre este Angel `porque en él está mi nombre`.

Deut 4:35-37
```
35 A ti te fue mostrado, para que supieras que el Señor, Él es Dios; ningún otro hay fuera de Él. 

36 Desde los cielos te hizo oír su voz para disciplinarte[n]; y sobre la tierra te hizo ver su gran fuego, 
y oíste sus palabras de en medio del fuego. 

37 Porque[o] Él amó a tus padres, 
por eso escogió a su descendencia[p] después de ellos; y personalmente[q](panim) te sacó de Egipto con su gran poder,
```

Panim: Presencia del rostro de Jehova Dios de Israel en hebreo.


Jueces 2

```
Y el ángel del Señor subió de Gilgal a Boquim y dijo: 
Yo os saqué de Egipto y os conduje a la tierra que había prometido a vuestros padres y dije: 
«Jamás quebrantaré mi pacto con vosotros, 
2 y en cuanto a vosotros, no haréis pacto con los habitantes de esta tierra; sus altares derribaréis». 
Pero vosotros no me habéis obedecido[a]; ¿qué es esto que habéis hecho? 

3 Por lo cual también dije: «No los echaré de delante de vosotros, 
sino que serán como espinas en vuestro costado[b], y sus dioses serán lazo para vosotros». 

4 Y sucedió que cuando el ángel del Señor habló estas palabras a todos los hijos de Israel, 
el pueblo alzó su voz y lloró. 5 Y llamaron a aquel lugar Boquim[c]; y allí ofrecieron sacrificio al Señor.
```

Todas estas fraces las dice el Angel en primera persona. `Y el ángel del Señor subió de Gilgal a Boquim`, 
`Yo os saqué de Egipto`, ` y dije: Jamás quebrantaré mi pacto con vosotros`, `Pero vosotros no me habéis obedecido[a]`.

Que es lo que esta diciendo el Angel?
De alguna el manera el Angel esta diciendo. Yo estube con ustedes por 40 anios, me veian todas las mananas, todos los dias,
estuve en el dicierto escuchando sus quejas y no me fui. Pero ustedes ahora se han tornado en idolatras, es el momento de irme.

Por primera vez en 40+ anios, el Angel ya no iba a estar con ellos.

Por este motivo lloraron y llamaron al lugar BOQUIM.
(heb. Bôkîm, “llorones [lloradores, lamentadores]” o “lugar del llanto”). 
Lugar cerca de Gilgal donde los israelitas lloraron por su desobediencia (Jdg 2:1-5); todavía no ha sido identificado.

Gen 31:10-13

```
10 Y sucedió que por el tiempo cuando el rebaño estaba en celo[a], alcé los ojos y vi en sueños; y he aquí, 
los machos cabríos que cubrían las hembras[b] eran rayados, moteados y abigarrados. 

11 Entonces el ángel de Dios me dijo en el sueño: «Jacob»; y yo respondí: «Heme aquí». 

12 Y él dijo: «Levanta ahora los ojos y ve que todos los machos cabríos que están cubriendo las hembras[c] son rayados, 
moteados y abigarrados, pues yo he visto todo lo que Labán te ha hecho. 

13 Yo soy el Dios de Betel, 
donde tú ungiste un pilar, donde me hiciste un voto. Levántate ahora, sal de esta tierra, 
y vuelve a la tierra donde naciste[d]».
```


. Betel, en hebreo (בית אל), significa "casa de Dios", es el nombre de una ciudad cananea de la antigua región de Samaria, 
situada en el centro de la tierra de Canaán, al noroeste de Ai por el camino para Siquem, 
a 30 kilómetros al sur de Silo y a unos 16 kilómetros al norte de Jerusalén. 
Betel es la segunda ciudad más mencionada en la Biblia. 
En este lugar fue donde Abraham construyó su altar cuando llegó por primera vez a Canaán 
(Génesis 12:8; 13:3). Allí Jacob vio en visión una escalera cuyo extremo tocaba el cielo y
los ángeles subían y bajaban Génesis 28:10-19. Por esta razón Jacob tuvo miedo, y dijo: 
"¡Cuán terrible es este lugar! No es otra cosa que casa de Dios, y puerta del cielo". 
y llamó Betel al lugar que era conocido como Liz (Génesis 35-15). Betel también era un santuario en los días del profeta Samuel, 
quién allí juzgaba al pueblo (1 Samuel 7:16; 10:3). Y fue el lugar donde fue sepultada Débora, la nodriza de Rebeca, 
esposa de Isaac. Betel fue el lugar de nacimiento de Hiel quien trató de reedificar la ciudad de Jericó (1 Reyes 16:34).


Gen 48:14-15

```
14 Pero Israel extendió su derecha y la puso sobre la cabeza de Efraín, que era el menor,
y su izquierda sobre la cabeza de Manasés, cruzando adrede sus manos, aunque[a] Manasés era el primogénito. 

15 Y bendijo a José, y dijo:

El Dios delante de quien anduvieron mis padres Abraham e Isaac,
el Dios que ha sido mi pastor toda mi vida[b] hasta este día,

16 el ángel que me ha rescatado de todo mal,
bendiga a estos muchachos;

y viva[c] en ellos mi nombre,
y el nombre de mis padres Abraham e Isaac;
y crezcan para ser multitud en medio de la tierra.

```

Israel dice `el ángel que me ha rescatado de todo mal,
bendiga a estos muchachos`

En este versiculo hay una funcion de Dios con el Angel.

```
El Dios delante de quien anduvieron mis padres Abraham e Isaac,
el Dios que ha sido mi pastor toda mi vida[b] hasta este día,
el ángel que me ha rescatado de todo mal,
bendiga a estos muchachos;
```

Jueces 6


```
11 Y vino el ángel del Señor y se sentó debajo de la encina[g] que estaba en Ofra, 
la cual pertenecía a Joás abiezerita; y su hijo Gedeón estaba sacudiendo el trigo en el lagar, 
para esconderlo[h] de los madianitas. 

12 Y el ángel del Señor se le apareció, y le dijo: El Señor está contigo, valiente guerrero. 

13 Entonces Gedeón le respondió: Ah señor mío, si el Señor está con nosotros, ¿por qué nos ha ocurrido todo esto? 
¿Y dónde están todas sus maravillas que nuestros padres nos han contado, diciendo: «¿No nos hizo el Señor subir de Egipto?». 
Pero ahora el Señor nos ha abandonado, y nos ha entregado en mano[i] de los madianitas.

14 Y el Señor lo miró[j], y dijo: Ve con esta tu fuerza, y libra a Israel de la mano[k] de los madianitas. 
¿No te he enviado yo? 

15 Y él respondió: Ah Señor, ¿cómo[l] libraré a Israel? 
He aquí que mi familia es la más pobre en Manasés, y yo el menor de la casa de mi padre. 

16 Pero el Señor le dijo: Ciertamente yo estaré contigo, y derrotarás[m] a Madián como a un solo hombre. 

17 Y Gedeón le dijo: Si he hallado gracia ante tus ojos, muéstrame una señal de que eres tú el que hablas conmigo. 

18 Te ruego que no te vayas de aquí hasta que yo vuelva a ti, y traiga mi ofrenda y la ponga delante de ti. 
Y él respondió: Me quedaré hasta que vuelvas.

19 Y Gedeón entró y preparó un cabrito y pan sin levadura de un efa[n] de harina; 
 puso la carne en una cesta y[o] el caldo en un caldero, y se los llevó a él debajo de la encina[p] y se los presentó. 
 20 Y el ángel de Dios le dijo: Toma la carne y el pan sin levadura, ponlos sobre esta peña y derrama el caldo. 
 Y así lo hizo. 
 
 21 Entonces el ángel del Señor extendió la punta de la vara que estaba en su mano y tocó la carne y el pan sin levadura; 
 y subió fuego de la roca que consumió la carne y el pan sin levadura. 
 Y el ángel del Señor desapareció[q] de su vista. 
 
 22 Al ver Gedeón que era el ángel del Señor, dijo[r]: 
 ¡Ay de mí, Señor Dios[s]! Porque ahora he visto al ángel del Señor cara a cara. 
 
 23 Y el Señor le dijo: La paz sea contigo, no temas; no morirás.

```

En el verso 18 en el angel, le dice que se va a quedar ahi hasta que el vuelva con su ofrenda.

Donde estaba el angel en el versiculo 11 `se sentó debajo de la encina[g] que estaba en Ofra` y donde puso la ofrenda Gedeon
en el verso 19 `Y Gedeón entró y preparó un cabrito y pan sin levadura de un efa[n] de harina; 
 puso la carne en una cesta y[o] el caldo en un caldero, y se los llevó a él debajo de la encina[p]`
 
 ## Resumen
 
* La Biblia hebrea contiene sugerencias claras de una Deidad (Yahweh como dos figuras)
 
* El "Nombre" es otra forma de referirse a Jeova "como los Judios lo hacen ahora"
 
* El Nombre está dentro del Ángel de Jeova.

* El ángel es, por tanto, Jeova en forma humana.

## La Palabra

Gen 15

```
Después de estas cosas la palabra del Señor vino a Abram en visión, diciendo:

No temas, Abram,
yo soy un escudo para ti;
tu recompensa será muy grande.
```

La palabra clave es vision, o sea que Abram estaba viendo no solo escuchando.

1 Sam 3

```
El joven Samuel servía[a] al Señor en presencia de Elí. 
La palabra del Señor escaseaba en aquellos días, las visiones no eran frecuentes[b]. 

2 Y aconteció un[c] día, estando Elí acostado en su aposento[d] 
(sus ojos habían comenzado a oscurecerse y no podía ver bien), 

3 cuando la lámpara de Dios aún no se había apagado y 
Samuel estaba acostado en el templo del Señor donde estaba el arca de Dios,

4 que el Señor llamó a Samuel, y él respondió: Aquí estoy. 5 Entonces corrió a Elí y le dijo:
Aquí estoy, pues me llamaste. Pero Elí[e] le respondió: Yo no he llamado, vuelve a acostarte. 
Y él fue y se acostó. 

6 El Señor volvió a llamar: ¡Samuel! Y Samuel se levantó, fue a Elí y dijo: 
Aquí estoy, pues me llamaste. Pero él respondió: Yo no he llamado, hijo mío, vuelve a acostarte. 
7 Y Samuel no conocía aún al Señor, ni se le había revelado aún la palabra del Señor. 

8 El Señor volvió a llamar a Samuel por tercera vez. Y él se levantó, fue a Elí y dijo:
Aquí estoy, pues me llamaste. Entonces Elí comprendió que el Señor estaba llamando al muchacho. 

9 Y Elí dijo a Samuel: Ve y acuéstate, y[f] si Él te llama, dirás: 
«Habla, Señor, que tu siervo escucha». Y Samuel fue y se acostó en su aposento[g].

10 Entonces vino el Señor y se detuvo, y llamó como en las otras ocasiones: 
¡Samuel, Samuel! Y Samuel respondió: Habla, que tu siervo escucha.

19 Samuel creció, y el Señor estaba con él; no dejó sin cumplimiento[i] ninguna de sus palabras. 

20 Y todo Israel, desde Dan hasta Beerseba, supo que Samuel había sido confirmado como profeta del Señor. 
21 Y el Señor se volvió a aparecer en Silo; porque el Señor se revelaba a Samuel en Silo por la palabra del Señor.

```

Todo isral sabia que Samuel era el profeta de Dios, por que se revelaba a Samuel como la palabra.

Juan 1:1

```
1 En el principio existía[a] el Verbo[b], y el Verbo estaba[c] con Dios, y el Verbo era Dios. 
```

Jeremias 1

```
 Y vino a mí la palabra del Señor, diciendo:

5 Antes que yo te formara en el seno materno, te conocí,
y antes que nacieras, te consagré,
te puse por profeta a las naciones.
6 Entonces dije: ¡Ah, Señor Dios[a]!
He aquí, no sé hablar,
porque soy joven.
7 Pero el Señor me dijo:
No digas: «Soy joven»,
porque adondequiera que te envíe, irás,
y todo lo que te mande, dirás.
8 No tengas temor ante ellos,
porque contigo estoy para librarte —declara el Señor.

9 Entonces extendió el Señor su mano y tocó mi boca. Y el Señor me dijo:

He aquí, he puesto mis palabras en tu boca.
```

## El que cabalga sobre las nubes

Uno de los titulos de Baal es "El que cabalga sobre las nubes"


Lo que hicieron los escritores biblicos en 5 ocaciones es que usaron ese titulo, identicamente o
sercanamente identicamente, y lo usaron para describir al Dios de de Israel.

Entonces Jeova y Baal son el mismo?
No, la conclusion es que los escritores de la Biblia que tomaron este nombre, lo tomaron para decir que Baal
no es el que cabalga sobre las nubes. Es Jeova el que lo hace. Jeova es que el verdaderemente es soberano sobre todo; 
no es baal.

Deut 33

```
26 No hay como el Dios de Jesurún,
Quien cabalga sobre los cielos para tu ayuda,
Y sobre las nubes con su grandeza.
```

Salmo 68

```
32 Cantad a Dios, oh reinos de la tierra;
cantad alabanzas al Señor. (Selah)
33 Cantad al que cabalga sobre los cielos de los cielos, que son desde la antigüedad;
he aquí, Él da su voz, voz poderosa.
```

Salmo 104

```
Bendice, alma mía, a Jehová.
    Jehová Dios mío, mucho te has engrandecido;
    Te has vestido de gloria y de magnificencia.

2 El que se cubre de luz como de vestidura,
Que extiende los cielos como una cortina,

3 Que establece sus aposentos entre las aguas,
El que pone las nubes por su carroza,
El que anda sobre las alas del viento;

```

Isa 19

```
Profecía sobre[a] Egipto.
He aquí, el Señor va montado sobre una nube veloz y llega a Egipto;
se estremecen los ídolos de Egipto ante su presencia,
y el corazón de los egipcios se derrite dentro de ellos.

```

Todos estos pasajes describen a Jeova, la unica exepcion es:

Daniel 7:9-13

```
Seguí mirando
hasta que se establecieron tronos,
y el Anciano de Días se sentó.
Su vestidura era blanca como la nieve,
y el cabello de su cabeza como lana pura,
su trono, llamas de fuego,
y sus ruedas, fuego abrasador.
10 Un río de fuego corría,
saliendo de delante de Él.
Miles de millares le servían,
y miríadas de miríadas estaban en pie delante de Él.
El tribunal se sentó,
y se abrieron los libros.

11 Entonces yo seguí mirando a causa del ruido de las palabras arrogantes[a] que el cuerno decía; 
seguí mirando hasta que mataron a la bestia, destrozaron su cuerpo y lo echaron a las llamas del fuego. 

12 A las demás bestias, se les quitó el dominio, pero les fue concedida una prolongación de la vida por un tiempo determinado.

13 Seguí mirando en las visiones nocturnas,
y he aquí, con las nubes del cielo
venía uno como un Hijo de Hombre,
que se dirigió al Anciano de Días
y fue presentado ante Él.
```

Entonces este que viene con las nubes del cielo es Jeova? Si lo es.

Mateo 26: 63-65

```
63 Mas Jesús callaba. Y el sumo sacerdote le dijo: 
Te conjuro por el Dios viviente que nos digas si tú eres el Cristo[a], el Hijo de Dios. 
64 Jesús le dijo*: Tú mismo lo has dicho; sin embargo, 
os digo que desde ahora veréis al Hijo del Hombre sentado a la diestra 
del Poder, y viniendo sobre las nubes del cielo. 
65 Entonces el sumo sacerdote rasgó sus vestiduras, diciendo: ¡Ha blasfemado! 
¿Qué necesidad tenemos de más testigos? He aquí, ahora mismo habéis oído la blasfemia;
```














